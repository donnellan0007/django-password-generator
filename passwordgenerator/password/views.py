from django.shortcuts import render

# Create your views here.
def strong_password(request):
    import random
    random_words = [
        'head',
        'watermelon',
        'python',
        'animals',
        'power',
        'guitar',
        'smart',
        'biscuit',
        'code',
        'utopia',
        'cousin',
        'priest',
        'muncher',
        'grand',
        'skipper',
        'winner',
        'royale',
        'victory',
        'chicken',
        'broken',
        'trick',
        'sobre'
    ]
    random_word = random_words[random.randrange(len(random_words))]
    random_word2 = random_words[random.randrange(len(random_words))]
    random_word3 = random_words[random.randrange(len(random_words))]
    random_number = random.randint(1,1000)
    symbols = ['!','#','$','%','^','&','*','(',')','-','=','+',';','/','.']
    letters = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z']
    random_symbol = symbols[random.randrange(len(symbols))]
    strong_password_gen = random_word.capitalize() + random_word2 + str(random_number) + random_symbol + random_word3.capitalize() + random_words[random.randrange(len(random_words))] + letters[random.randrange(len(letters))] + str(random.randint(1000,9999))

    return render(request,'password/strong.html', {'strong_password_gen':strong_password_gen})

def weak_password(request):
    import random
    random_words = [
        'head',
        'watermelon',
        'python',
        'animals',
        'power',
        'guitar',
        'smart',
        'biscuit',
        'code',
        'utopia',
        'cousin',
        'priest',
        'muncher',
        'grand',
        'skipper',
        'winner',
        'royale',
        'victory',
        'chicken',
        'broken',
        'trick',
        'sobre'
    ]
    random_word = random_words[random.randrange(len(random_words))]
    random_word2 = random_words[random.randrange(len(random_words))]
    random_word3 = random_words[random.randrange(len(random_words))]
    random_number = random.randint(1,1000)
    symbols = ['!','#','$','%','^','&','*','(',')','-','=','+',';','/','.']
    letters = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z']
    random_symbol = symbols[random.randrange(len(symbols))]
    random_password = random_word.capitalize() + random_word2 + str(random_number) + random_word3

    return render(request,'password/index.html', {'random_password':random_password})